const fs = require('fs');

function saveToFile(fileName, data) {
    try {
        fs.writeFileSync(fileName, JSON.stringify(data), 'utf8');
    }
    catch (err) {
        console.error(err);
    }
}

function sumOfEyes(data) {
    let obj = {"brown": 0, "green": 0, "blue": 0}
    data.forEach(person => obj[person.eyeColor] += 1);
    saveToFile('homework5-1_eyes.json', obj);
    return obj;
}

function userFriendCount(data) {
    let people = [];
    data.forEach(person => {
        let obj = {};
        obj["_id"] = person._id;
        obj["friendCount"] = person.friends.length;
        people.push(obj);
    });
    saveToFile('homework5-1_friends.json', people);
    return people; 
}

function sumOfGender(data) {
    let obj = {"male": 0, "female": 0}
    data.forEach(person => obj[person.gender] += 1);
    saveToFile('homework5-1_gender.json', obj);
    return obj;
}

module.exports = {
    sumOfEyes,
    userFriendCount,
    sumOfGender
};
